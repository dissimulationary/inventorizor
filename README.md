# Inventorizor

Inventorizor is a script for dealing with Star Citizen inventory transfers. It will automatically click and drag inventory items right to left or left to right depending on settings. Due to fun with Xorg, it is best that every user set their own left and right cursor postions rather than having presets. This must be done before the script will do anything and can be re-set at any time. Doing so will create the file inventorizor.cfg in the ~/.config directory. The script with its options should be set to keyboard shortcuts as ALT-TABing out of Star Citizen running in Linux is not a great idea (at least for me).

In game, make sure that the side you are dragging to is filtered so that nothing is showing. Draging and droping onto an existing item can have varied results. Dragging and droping onto empty space seems to work very consistently.

***

## Dependencies

**xdotool**

Debian/Ubuntu/Mint/Pop!_OS/elementary/etc

```
sudo apt install xdotool
```

Arch/Manjaro/Garuda/etc

```
pacman -S xdotool
```

Fedora

```
dnf install xdotool
```

OpenSUSE

```
sudo zypper install xdotool
```

## Installation

Download the script and read it (never blindly run anything you get online). Once you are happy that it won't melt anything you love, make it executable (preferably somewhere in your PATH). You then need to set up keyboard shortcuts that you can use in game. Google how to do this for your particular distro.

## Usage

```
inventorizor.sh [-l | -r] | [-d <ltr|rtl>] [-x <n>]
```

## Options
```
    -h              Print help text
    -l              Save left cursor position
    -r              Save right cursor position
    -d <ltr|rtl>    Specify direction left to right (ltr) or (default) right to left (rtl)    
    -x <n>          execute for n specified number of loops
```

## Example Shortcuts

* **CTRL+ALT+L** = inventorizor.sh -l (set the *left* cursor coordinates)
* **CTRL+ALT+R** = inventorizor.sh -r (set the *right* cursor coordinates)
* **CTRL+ALT+1** = inventorizor.sh -d rtl -x 10 ( **<<<** move 10 items from *right* to *left*)
* **CTRL+ALT+2** = inventorizor.sh -d rtl -x 20 ( **<<<** move 20 items from *right* to *left*)
* **CTRL+SHIFT+1** = inventorizor.sh -d ltr -x 10 ( **>>>** move 10 items from *left* to *right*)
* **CTRL+SHIFT+2** = inventorizor.sh -d ltr -x 20 ( **>>>** move 20 items from *left* to *right*)

    You can of course set up any of your own custom number counts such as:

* **CTRL+ALT+[n]** = inventorizor.sh -d rtl -x [n]0 (move [n]0 items from right to left)

    Or really anything you want. Be creative!

## License
[GNU General Public License, version 2](https://www.gnu.org/licenses/old-licenses/gpl-2.0.en.html)
