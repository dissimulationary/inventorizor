#!/bin/bash

configFile="$HOME/.config/inventorizor.cfg"
direction="rtl"
usage="Inventorizor\n\nUsage: inventorizor.sh [-l | -r] | [-d <ltr|rtl>] [-x <n>]\n\n
Inventorizor is a script for dealing with Star Citizen inventory transfers. It will automatically\n
click and drag inventory items right to left or left to right depending on settings. Due to fun \n
with Xorg, it is best that every user set their own left and right cursor postions rather than\n
having presets. This must be done before the script will do anything and can be re-set at any\n
time. Doing so will create the file inventorizor.cfg in the ~/.config directory. The script with\n
its options should be set to keyboard shortcuts as ALT-TABing out of Star Citizen running in Linux\n
is not a great idea (at least for me).\n\n
In game, make sure that the side you are dragging to is filtered so that nothing is showing.\n
Draging and droping onto an existing item can have varied results. Dragging and droping onto empty\n
space seems to work very consistently.\n\n
Here are some suggestions:\n\n
\tCTRL+ALT+L = inventorizor.sh -l (set the left cursor coordinates)\n
\tCTRL+ALT+R = inventorizor.sh -r (set the right cursor coordinates)\n
\tCTRL+ALT+1 = inventorizor.sh -d rtl -x 10 ( <<< move 10 items from right to left)\n
\tCTRL+SHIFT+1 = inventorizor.sh -d ltr -x 10 ( >>> move 10 items from left to right)\n\n
\tYou can of course set up any of your own custom number counts such as:\n\n
\tCTRL+ALT+[n] = inventorizor.sh -d rtl -x [n]0 (move [n]0 items from right to left)\n\n
Options:\n\n
   -h\t\tPrint help text\n
   -l\t\tSave left cursor position\n
   -r\t\tSave right cursor position\n
   -d <ltr|rtl>\tSpecify direction left to right (ltr) or (default) right to left (rtl)\n
   -x <n>\t\texecute for n specified number of loops"

while getopts ":hlrd:x:" opt; do
  case $opt in
    h)
      echo -e $usage
      exit 0
      ;;
    l)
      echo "updating left cursor coordinates" >&2
      eval $(xdotool getmouselocation --shell)
      lx=$X
      ly=$Y
      if test -f "$configFile"; then
        sed -i -e "/leftX=/ s/=.*/=$lx/" "$configFile"
        sed -i -e "/leftY=/ s/=.*/=$ly/" "$configFile"
      else
        echo "leftX=$lx" >> $configFile
        echo "leftY=$ly" >> $configFile
        echo "rightX=unset" >> $configFile
        echo "rightY=unset" >> $configFile
      fi
      exit 0
      ;;
    r)
      echo "updating right cursor coordinates" >&2
      eval $(xdotool getmouselocation --shell)
      rx=$X
      ry=$Y
      if test -f "$configFile"; then
        sed -i -e "/rightX=/ s/=.*/=$rx/" "$configFile"
        sed -i -e "/rightY=/ s/=.*/=$ry/" "$configFile"
      else
        echo "leftX=unset" >> $configFile
        echo "leftY=unset" >> $configFile        
        echo "rightX=$rx" >> $configFile
        echo "rightY=$ry" >> $configFile
      fi
      exit 0
      ;;
    d)
      if [ $OPTARG="ltr" -o $OPTARG="rtl" ]; then
        direction=$OPTARG
        echo "direction selected: $OPTARG" >&2
      else
        echo "direction must be either 'ltr' or 'rtl' exactly" >&2
        exit 1
      fi
      ;;
    x)
      #make sure that the left and right cursor positions have been set
      if [[ ! -f "$configFile" ]] || grep -Fq "unset" $configFile; then
        echo 'you must first run the script separately with the -l AND -r flags to set both' >&2
        echo 'left and right cursor positions before you can use it' >&2
        exit 1
      else
        source $configFile
      fi
      #confirm argument is an integer
      re='^[0-9]+$'
      if ! [[ $OPTARG =~ $re ]]; then
        echo "value must be an integer" >&2
        exit 1
      else
        echo "executing move for $OPTARG loops" >&2      
        if [ $direction = "rtl" ]; then        
          for (( i=1; i<=$OPTARG; i++ )); do
            xdotool \
            mousemove --sync $rightX $rightY sleep 0.2 \
            mousedown 1 sleep 0.2 \
            mousemove --sync $leftX $leftY sleep 0.2 \
            mouseup 1 sleep 0.2
            #echo "loop right to left #$i"
          done
        else
          for (( i=1; i<=$OPTARG; i++ )); do
            xdotool \
            mousemove --sync $leftX $leftY sleep 0.2 \
            mousedown 1 sleep 0.2 \
            mousemove --sync $rightX $rightY sleep 0.2 \
            mouseup 1 sleep 0.2
            #echo "loop left to right #$i"
          done
        fi
      fi
      exit 0
      ;;
    \?)
      echo "Invalid option: -$OPTARG" >&2
      echo -e $usage
      exit 1
      ;;
    :)
      echo "Option -$OPTARG requires an argument." >&2
      exit 1
      ;;
  esac
done

echo -e $usage
exit 1
